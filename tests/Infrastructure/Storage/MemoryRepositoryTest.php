<?php

namespace Infrastructure\Storage;

use Insidesuki\DDDUtils\Infrastructure\Storage\MemoryRepository;
use PHPUnit\Framework\TestCase;

class MemoryRepositoryTest extends TestCase
{

	public function testEmptyRepository(){

		$repo = new EntityMemoryRepository();
		$all = $repo->findAll();

		$this->assertIsArray($all);
		$this->assertCount(0,$all);


	}

	public function testFullRepository(){
		$repo = new EntityMemoryRepository(true);
		$all = $repo->findAll();
		$this->assertCount(2,$all);

	}

	public function testSimulateResponse(){

		$returns = [
			'findById' => null
		];

		$repo = new EntityMemoryRepository(true,$returns);
		$entity = $repo->findById('asd');
		$this->assertNull($entity);

	}




}


class EntityMemoryRepository extends MemoryRepository
{


	public static function create(): array
	{
		return [
			new Entity('test1'),
			new Entity('test2')
		];
	}

	public function findAll():array{

		return $this->return(__FUNCTION__,true);

	}

	public function findById(string $id): ?Entity
	{
		return $this->return(__FUNCTION__);
	}

	public function findByName(string $name): ?Entity
	{

		return $this->return(__FUNCTION__);

	}
}


class Entity
{

	private string $idEntity;
	private string $name;


	public function __construct(string $name)
	{
		$this->idEntity = sha1(rand(2,100));
		$this->name     = $name;
	}

	public function idEntity(): string
	{
		return $this->idEntity;
	}

	public function name(): string
	{
		return $this->name;
	}


}