<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Infrastructure\Storage;

abstract class MemoryRepository
{

	protected array $storage = [];

	/**
	 * @var array
	 */
	protected array $returns;


	public function __construct(bool $full = false, array $returns = [])
	{
		$this->returns = $returns;
		if($full) {
			$this->createEntities();
		}


	}


	public function createEntities(): void
	{

		$entities = $this->create();
		foreach ($entities as $entity) {
			$this->add($entity);
		}

	}


	public function return(string $method, bool $array = false): mixed
	{


		if(array_key_exists($method, $this->returns)) {
			return $this->returns[$method];
		}
		return ($array) ? $this->storage : $this->search();

	}


	abstract public static function create(): array;

	protected function search()
	{
		return (!empty($this->storage)) ? $this->storage[0] : null;
	}

	/**
	 * @param object $object
	 * @return void
	 */
	protected function add(object $object): void
	{
		$this->storage[] = $object;
	}

	protected function remove($entity): void
	{

		unset($this->storage[$entity]);

	}


	public function getReturns()
	{
		return $this->returns;
	}

}