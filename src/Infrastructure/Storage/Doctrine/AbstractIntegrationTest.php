<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine;

use Doctrine\Persistence\ManagerRegistry;
use Insidesuki\DDDUtils\DataTests\Adapters\DoctrineAdapterDataTest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Insidesuki\DDDUtils\DataTests\DataTest;
abstract class AbstractIntegrationTest extends KernelTestCase
{


	protected                    $kernelTest;
	protected ContainerInterface $containerTest;
	protected ?ManagerRegistry   $entityManager;

	private array $dataTests = [];

	public function setUp(): void
	{
		$this->kernelTest    = self::bootKernel();
		$this->containerTest = $this->kernelTest->getContainer();

	}

	private function getEntityManager()
	{

		if(!isset($this->entityManager)) {
			$this->entityManager = $this->getService('doctrine');
		}
		return $this->entityManager;


	}

	protected function getService(string $fqns): mixed
	{

		return $this->containerTest->get($fqns);

	}


	protected function getDoctrineAdapter(){

		return new DoctrineAdapterDataTest($this->getEntityManager());

	}



	protected function createDataTest(string $table,array $data = []):DataTest
	{
		$dataTest = new DataTest($table,$this->getDoctrineAdapter());
		if(!empty($data)){
			$dataTest->addData($data);
		}

		$this->dataTests[$table] = $dataTest;

		return $dataTest;

	}

	protected function getDataTest(string $tableName):DataTest{

		return $this->dataTests[$tableName];

	}

	/**
	 * Delete al dataTests
	 * @return void
	 */
	public function tearDown(): void
	{
		/**
		 * @var DataTest $dataTest
		 */

		$dataTestsReverse = array_reverse($this->dataTests);
		foreach($dataTestsReverse AS $dataTest){

			$dataTest->flush();

		}

		parent::tearDown();

	}




}