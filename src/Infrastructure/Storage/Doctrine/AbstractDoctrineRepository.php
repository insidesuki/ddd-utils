<?php

namespace Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine;

use Doctrine\DBAL\Result;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

/**
 *
 */
abstract class AbstractDoctrineRepository
{
	protected $objectRepository;

	/**
	 * @var null entityManager for different connection
	 */
	protected $entityManager = null;
	/**
	 * @var ManagerRegistry
	 */
	private $managerRegistry;


	public function __construct(
		ManagerRegistry $managerRegistry
	)
	{
		$this->managerRegistry  = $managerRegistry;
		$this->objectRepository = $this->getEntityManager()->getRepository($this->entityClass());
	}


	/**
	 * @return EntityManagerInterface
	 */
	protected function getEntityManager(): EntityManagerInterface
	{

		$entityManager = $this->managerRegistry->getManager($this->entityManager);
		if($entityManager->isOpen()) {
			return $entityManager;
		}
		return $this->managerRegistry->resetManager();
	}

	/**
	 * @return string
	 */
	abstract protected static function entityClass(): string;


	/**
	 * @param object $entity
	 * @param bool $flush
	 */
	protected function saveEntity(object $entity, bool $flush = true): void
	{
		$this->getEntityManager()->persist($entity);
		if($flush) {
			$this->getEntityManager()->flush();
		}

	}

	/**
	 * @param object $entity
	 * @param bool $flush
	 */
	protected function removeEntity(object $entity, bool $flush = true): void
	{
		$this->getEntityManager()->remove($entity);
		if($flush) {
			$this->getEntityManager()->flush();
		}

	}

	/**
	 * Run raw query
	 * @param string $query
	 * @param array $params
	 * @return Result
	 * @throws \Doctrine\DBAL\Exception
	 */
	protected function executeRawQuery(string $query,array $params = []): Result
	{

		return $this->getEntityManager()->getConnection()->executeQuery($query,$params);

	}

	/**
	 * Execute native query with resultmapping from entity class
	 * @param string $query
	 * @param array $params
	 * @param string $alias
	 * @return NativeQuery
	 */
	protected function executeNativeQuery(string $query, array $params = [],string $alias = 'A'): NativeQuery
	{

		$dcmQuery = $this->getEntityManager()->createNativeQuery($query, $this->getResultMapping($alias));
		foreach ($params as $key => $value) {
			$dcmQuery->setParameter($key, $value);
		}
		return $dcmQuery;

	}

	/**
	 * Get resultMapping to entityClass
	 * @param string $alias
	 * @return ResultSetMapping
	 */
	protected function getResultMapping(string $alias = 'a'): ResultSetMapping
	{

		$resultMappging = new ResultSetMappingBuilder($this->getEntityManager());
		$resultMappging->addRootEntityFromClassMetadata($this->entityClass(), $alias);
		return $resultMappging;


	}


	protected function findById(mixed $id)
	{

		return $this->objectRepository->find($id);

	}

	protected function findOne(array $criteria)
	{

		return $this->objectRepository->findOneBy($criteria);

	}

}
