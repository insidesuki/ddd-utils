<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\DataTests;

use Insidesuki\DDDUtils\DataTests\Contracts\AdapterDataInterface;

class DataTest
{

	private string $tableName;

	private array $datas;

	private AdapterDataInterface $adapter;

	public function __construct(string $tableName, AdapterDataInterface $runner)
	{
		$this->tableName = $tableName;
		$this->adapter   = $runner;
	}

	public function addData(array $data, bool $run = true): void
	{
		$this->adapter->delete($this->tableName,$data);
		if($run) {
			$this->adapter->insert($this->tableName, $data);
		}
		$this->datas[] = $data;
	}

	public function flush(): void
	{

		foreach ($this->datas as $data) {

			$this->adapter->delete($this->tableName, $data);

		}
		$this->datas = [];

	}

	public function run(): void
	{
		foreach ($this->datas as $data) {
			$this->adapter->insert($this->tableName, $data);
		}


	}


	public function tableName(): string
	{
		return $this->tableName;
	}

	public function datas(): array
	{
		return $this->datas;
	}


}