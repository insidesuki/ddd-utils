<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\DataTests\Adapters;

use Doctrine\DBAL\Connection;
use Insidesuki\DDDUtils\DataTests\Contracts\AdapterDataInterface;

class DoctrineAdapterDataTest implements AdapterDataInterface
{

	private $doctrine;


	public function __construct($doctrine)
	{

		$this->doctrine = $doctrine;
	}

	public function getConnection():Connection{

		return $this->doctrine->getConnection();

	}



	public function delete(string $table, array $params): mixed
	{

		return $this->getConnection()->delete($table,$params);

	}

	public function runRawQuery(string $query, bool $all = false): mixed
	{

		$sql  = $this->getConnection()->executeQuery($query);
		if($all) {
			return $sql->fetchAllAssociative();
		}
		return $sql->fetchOne();


	}

	public function update(string $table, array $data,array $criteria): mixed{

		return $this->getConnection()->update($table,$data,$criteria);
	}

	public function insert(string $table, array $data): mixed
	{

		return $this->getConnection()->insert($table,$data);


	}


}