<?php

namespace Insidesuki\DDDUtils\DataTests\Contracts;
interface AdapterDataInterface
{

	public function delete(string $table, array $params):mixed;

	public function insert(string $table, array $data):mixed;

	public function update(string $table, array $data,array $criteria):mixed;


}