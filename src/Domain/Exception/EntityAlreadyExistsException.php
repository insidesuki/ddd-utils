<?php

namespace Insidesuki\DDDUtils\Domain\Exception;

use RuntimeException;

class EntityAlreadyExistsException extends RuntimeException
{
    public function __construct($message)
    {
        parent::__construct(sprintf('"%s", already exists!!', $message));
    }

}