<?php

namespace Insidesuki\DDDUtils\Domain\Exception;

use RuntimeException;


class EntityDoesNotExistsException extends RuntimeException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('"%s", does not exists!!', $message));
    }

}