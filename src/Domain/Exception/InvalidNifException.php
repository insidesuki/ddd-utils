<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Exception;

use RuntimeException;

class InvalidNifException extends RuntimeException
{

    public function __construct(string $message)
    {
        parent::__construct(sprintf('Invalid NIF "%s"',$message));
    }

}