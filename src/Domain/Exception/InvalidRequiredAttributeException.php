<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Exception;

use InvalidArgumentException;

class InvalidRequiredAttributeException extends InvalidArgumentException
{
    public function __construct(string $attr, string $value)
    {
        parent::__construct(sprintf('Invalid value "%s" for attr "%s"', $attr, $value), 422);
    }
}