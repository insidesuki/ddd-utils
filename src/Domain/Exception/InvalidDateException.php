<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Exception;
use InvalidArgumentException;

class InvalidDateException extends InvalidArgumentException
{

}