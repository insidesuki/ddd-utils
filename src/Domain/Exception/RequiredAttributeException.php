<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Exception;

use InvalidArgumentException;


class RequiredAttributeException extends InvalidArgumentException
{
    public function __construct(string $attr)
    {
        parent::__construct(sprintf('Attribute "%s", is required!!!',$attr));
    }
}