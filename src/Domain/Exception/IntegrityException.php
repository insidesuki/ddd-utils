<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Exception;

use RuntimeException;

class IntegrityException extends RuntimeException
{
    public function __construct($message)
    {
        parent::__construct(sprintf('InvalidState of "%s"',$message), 422);
    }
}