<?php


declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain;

use JsonSerializable;
use ReflectionClass;

/**
 * DtoSerializerAbstract Class
 */
abstract class DtoSerializer implements JsonSerializable
{

    public static function createFromArray(array $data): static
    {
        return (new ReflectionClass(static::class))->newInstanceArgs($data);
    }


    /**
     * deprecate in future versions
     * @param array $data
     * @return void
     */
    public function populate(array $data)
    {
        foreach ($data as $k => $v) {
            $this->__set($k, $v);
        }
    }


    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            // build the setter
            $mutator = $this->mutator($name, 'set');
            // if exist the setter
            if (method_exists($this, $mutator) and is_callable([$this, $mutator])) {
                $this->$mutator($value);
            }
        }

        return $this;
    }

    /**
     * @param $name
     * @param string $method
     * @return string
     */
    private function mutator($name, string $method = 'set'): string
    {
        $attr_parts = explode('_', $name);
        // build the mutator
        $mutator = $method . ucfirst($attr_parts[0]);
        $mutator .= (isset($attr_parts[1])) ? ucfirst($attr_parts[1]) : '';

        return $mutator;
    }


    public function jsonSerialize(): mixed
    {
        return $this->__toArray();
    }

    public function __toArray(): array
    {
        $reflextion = new ReflectionClass(static::class);
        $properties = $reflextion->getProperties();
        $data = [];
        foreach ($properties as $property):
            if (!empty($property->getValue($this))) {
                $data[$property->name] = $property->getValue($this);
            }

        endforeach;
        return $data;
    }


}