<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain;
class AbstractDataValidator
{

    protected $errors = [];
    protected $data = [];

    
    public function __construct(array $data = []){
        $this->data = $data;
    }

    public function isValid(): bool
    {

        return empty($this->errors);
    }


    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $context
     * @param string $desc
     * @return void
     */
    protected function addError(string $context, string $desc)
    {
        $this->errors[$context] = $desc;
    }

}

