<?php
declare(strict_types=1);
namespace Insidesuki\DDDUtils\Domain\Traits\Log;

use DateTime;
use DateTimeImmutable;

trait TimestampTrait
{

    private $createdOn;
    private $updatedOn;

    /**
     */
    private function setCreatedOn(): void
    {
        $this->createdOn = new DateTimeImmutable('now');
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }

    /**
     */
    private function setUpdatedOn(): void
    {
        $this->updatedOn = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getUpdatedOn():DateTime
    {
        return $this->updatedOn;
    }




}