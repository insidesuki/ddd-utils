<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Traits\Log;

trait RemoteAddressTrait
{

    private $ipAddress = '127.0.0.1';

    private function setRemoteAddress(): void
    {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $this->ipAddress = filter_var($_SERVER['REMOTE_ADDR'],FILTER_SANITIZE_SPECIAL_CHARS);
        }

    }

    /**
     * @return string
     */
    public function getRemoteAddress(): string
    {
        return $this->ipAddress;
    }
}