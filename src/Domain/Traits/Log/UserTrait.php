<?php

namespace Insidesuki\DDDUtils\Domain\Traits\Log;

trait UserTrait
{

    private $user = 'unknown';

    public function setUser(string $user)
    {
        $this->user = $user;

    }

    public function getUser(): string
    {
        return $this->user;
    }

}