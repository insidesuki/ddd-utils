<?php
declare(strict_types=1);

namespace Insidesuki\DDDUtils\Domain\Traits\Log;

trait LogTrait
{

    use TimestampTrait;
    use AgentTrait;
    use RemoteAddressTrait;
    use UserTrait;
}