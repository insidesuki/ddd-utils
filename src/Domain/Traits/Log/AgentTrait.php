<?php
declare(strict_types=1);
namespace Insidesuki\DDDUtils\Domain\Traits\Log;

trait AgentTrait
{

    private $agent = 'unknown';

    public function getAgent(): string
    {
        return $this->agent;
    }

    private function setAgent(): void
    {
        // don't use filter_input(input_server). don't work on php-fpm
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $this->agent = filter_var($_SERVER['HTTP_USER_AGENT'],FILTER_SANITIZE_SPECIAL_CHARS);
        }
    }


}