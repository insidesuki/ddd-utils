<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\Domain\Event;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class EventStore
{

	public readonly string    $idEventStore;
	private DateTimeImmutable $occurredOn;
	private string            $eventName;
	private string            $event;

	public function __construct(DateTimeImmutable $ocurredOn, string $eventName, string $event)
	{
		$this->idEventStore = Uuid::v4()->toRfc4122();
		$this->occurredOn   = $ocurredOn;
		$this->eventName    = $eventName;
		$this->event        = $event;
	}

	public function occurredOn(): DateTimeImmutable
	{
		return $this->occurredOn;
	}

	public function event(): ?string
	{
		return $this->event;
	}


	public function eventName(): string
	{
		return $this->eventName;
	}


}