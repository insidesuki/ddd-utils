<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\Domain\Event;
use Insidesuki\DDDUtils\Domain\Event\Contracts\DomainEventSubscriberInterface;

/**
 * DomainEvents publisher
 */
class DomainEventPublisher
{

	private static $instance;
	private array $subscribers;
	private array $events   = [];

	private function __construct()
	{
		$this->subscribers = [];
	}

	public static function instance(): self
	{

		if(null === static::$instance) {
			static::$instance = new static();
		}
		return static::$instance;
	}

	public function subscribe(DomainEventSubscriberInterface $domainEventSubscriber): void
	{
		$this->subscribers[] = $domainEventSubscriber;
	}

	public function publish($event): void
	{
		foreach ($this->subscribers as $subscriber) {

			if($subscriber->isSubscribedTo($event)) {
				$subscriber->handle($event);
			}
		}
	}

	public function raise(object $event): void
	{
		$this->events[] = $event;
	}


	public function popEvents(): array
	{
		$events       = $this->events;
		$this->events = [];
		return $events;
	}

	public function event(string $eventClassName): ?object
	{
		$event = array_values(
			array_filter(
				$this->events,
				static fn ($event) => is_a($event, $eventClassName)
			)
		);

		return $event[0] ?? null;

	}


	public function allEvents(): array
	{
		return $this->events;
	}


	public function getSubscribers(): array
	{
		return $this->subscribers;
	}

}