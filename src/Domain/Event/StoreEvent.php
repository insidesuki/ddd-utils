<?php
declare(strict_types = 1);

namespace Insidesuki\DDDUtils\Domain\Event;
use Insidesuki\DDDUtils\Domain\Event\Contracts\EventPersistibleInterface;
use Insidesuki\DDDUtils\Domain\Event\Contracts\EventStoreRepositoryInterface;
use JsonException;

/**
 * store events in a repository
 */
class StoreEvent
{

	public function __construct(private readonly  EventStoreRepositoryInterface $eventStoreRepository){}

	/**
	 * @throws JsonException
	 */
	public function handle(EventPersistibleInterface $event): void
	{

		$this->eventStoreRepository->store(
			new EventStore(
				$event->occurredOn(),
				$event->name(),
				json_encode($event, JSON_THROW_ON_ERROR)
			)
		);

	}



}