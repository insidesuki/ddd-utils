<?php

namespace Insidesuki\DDDUtils\Domain\Event\Contracts;
interface DomainEventSubscriberInterface
{

	public function handle($domainEvent);

	public function isSubscribedTo($domainEvent);

}