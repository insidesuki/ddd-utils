<?php

namespace Insidesuki\DDDUtils\Domain\Event\Contracts;
use DateTimeImmutable;

interface EventPersistibleInterface
{

	public function occurredOn(): DateTimeImmutable;
	public function name():string;

}