<?php

namespace Insidesuki\DDDUtils\Domain\Event\Contracts;


use Insidesuki\DDDUtils\Domain\Event\EventStore;

interface EventStoreRepositoryInterface
{
	public function store(EventStore $eventStore): void;
}